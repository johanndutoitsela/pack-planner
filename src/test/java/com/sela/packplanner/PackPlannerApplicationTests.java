package com.sela.packplanner;

import com.sela.packplanner.model.item.Item;
import com.sela.packplanner.model.item.ItemRepository;
import com.sela.packplanner.model.pack.Pack;
import com.sela.packplanner.model.pack.PackRepository;
import com.sela.packplanner.model.packplan.PackPlan;
import com.sela.packplanner.model.packplan.PackPlanRepository;
import com.sela.packplanner.model.packplan.PackSortOrder;
import com.sela.packplanner.model.packplanitem.PackPlanItem;
import com.sela.packplanner.model.packplanitem.PackPlanItemRepository;
import com.sela.packplanner.model.packsublot.PackSublot;
import com.sela.packplanner.model.packsublot.PackSublotRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PackPlannerApplicationTests {
    @Autowired
    private PackPlanRepository packPlanRepository;

    @Autowired
    private PackPlanItemRepository packPlanItemRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private PackRepository packRepository;

    @Autowired
    private PackSublotRepository packSublotRepository;
    @Test
    public void createSampleObjectGraph() {
        PackPlan packPlan = new PackPlan(PackSortOrder.NATURAL, 40, 500.0);
        this.packPlanRepository.save(packPlan);

        Item item1001 = new Item("1001", 6200, 9.653);
        Item item2001 = new Item("2001", 7200, 11.21);

        this.itemRepository.save(item1001);
        this.itemRepository.save(item2001);

        PackPlanItem packPlanItem1001 = new PackPlanItem(packPlan, item1001, 30);
        PackPlanItem packPlanItem2001 = new PackPlanItem(packPlan, item2001, 50);

        this.packPlanItemRepository.save(packPlanItem1001);
        this.packPlanItemRepository.save(packPlanItem2001);

        List<PackPlanItem> packPlanItems = this.packPlanItemRepository.findByPackPlan(packPlan);
        List<PackPlan> all = this.packPlanRepository.findAll();

        final Pack pack1 = this.packRepository.save(new Pack(1, packPlan));
        final Pack pack2 = this.packRepository.save(new Pack(2, packPlan));

        final PackSublot pack1Sublot1 = this.packSublotRepository.save(new PackSublot(pack1, 1, item1001, 30));
        final PackSublot pack1Sublot2 = this.packSublotRepository.save(new PackSublot(pack1, 2, item2001, 10));

        final PackSublot pack2Sublot1 = this.packSublotRepository.save(new PackSublot(pack2, 1, item2001, 40));

        final List<Pack> all1 = this.packRepository.findAll();
        final List<PackSublot> packSublots = this.packSublotRepository.findByPack(pack1);
        System.out.println();
    }

}

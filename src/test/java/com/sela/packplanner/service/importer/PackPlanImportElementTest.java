package com.sela.packplanner.service.importer;

import org.junit.Test;
import org.simpleflatmapper.csv.CsvParser;
import org.simpleflatmapper.util.CloseableIterator;
import sun.plugin.dom.exception.InvalidStateException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class PackPlanImportElementTest {

    @Test
    public void importElement() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("InputElements_10.csv").getFile());
        final FileReader fileReader = new FileReader(file);

        CloseableIterator<PackPlanImportElement> iterator = CsvParser.mapTo(PackPlanImportElement.class)
                .headers("SORT_ORDER", "MAX_PIECES_PER_PACK", "MAX_WEIGHT_PER_PACK")
                .iterator(file);
        final PackPlanImportElement packPlanImportElement = iterator.next();

        final List<ItemImportElement> itemImportElements = CsvParser.skip(1)
                .mapTo(ItemImportElement.class)
                .headers("ITEM_ID", "ITEM_LENGTH", "PACK_PLAN_ITEM_QUANTITY", "ITEM_WEIGHT")
                .stream(fileReader)
                .collect(Collectors.toList());

        if (!packPlanImportElement.isValid()) {
            throw new InvalidStateException("Invalid PackPlanImportElement: " + packPlanImportElement.toString());
        }

        for (ItemImportElement itemImportElement : itemImportElements) {
            if (!itemImportElement.isValid()) {
                throw new IllegalStateException("Invalid ItemImportElement: " + itemImportElement.toString());
            }
        }

        System.out.println();
    }

}
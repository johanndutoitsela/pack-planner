package com.sela.packplanner.service.importer;

import com.sela.packplanner.model.item.Item;
import com.sela.packplanner.model.item.ItemRepository;
import com.sela.packplanner.model.packplan.PackPlan;
import com.sela.packplanner.model.packplan.PackPlanRepository;
import com.sela.packplanner.model.packplanitem.PackPlanItem;
import com.sela.packplanner.model.packplanitem.PackPlanItemRepository;
import org.simpleflatmapper.csv.CsvParser;
import org.simpleflatmapper.util.CloseableIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import sun.plugin.dom.exception.InvalidStateException;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CsvPackPlannerImportService implements PackPlannerImportService {

    @Autowired
    PackPlanRepository packPlanRepository;

    @Autowired
    PackPlanItemRepository packPlanItemRepository;

    @Autowired
    ItemRepository itemRepository;

    @Override
    @Transactional
    public void importPackPlan(File file) throws IOException {
        final PackPlanImportElement packPlanImportElement = parseForPackPlanImportElement(file);
        final PackPlan packPlan = mapToPackPlan(packPlanImportElement);
        this.packPlanRepository.save(packPlan);

        final List<ItemImportElement> itemImportElements = parseForItemImportElements(file);
        final List<Pair<PackPlanItem, Item>> packPlanItemAndItems = mapToPackPlanItemAndItems(packPlan, itemImportElements);

        for (Pair<PackPlanItem, Item> packPlanItemAndItem : packPlanItemAndItems) {
            this.itemRepository.save(packPlanItemAndItem.getSecond());
            this.packPlanItemRepository.save(packPlanItemAndItem.getFirst());
        }
    }

    private PackPlanImportElement parseForPackPlanImportElement(File file) throws IOException {
        CloseableIterator<PackPlanImportElement> iterator = CsvParser.mapTo(PackPlanImportElement.class)
                .headers(PackPlanImportHeader.SORT_ORDER.name(),
                        PackPlanImportHeader.MAX_PIECES_PER_PACK.name(),
                        PackPlanImportHeader.MAX_WEIGHT_PER_PACK.name())
                .iterator(file);

        final PackPlanImportElement packPlanImportElement = iterator.next();
        iterator.close();
        if (!packPlanImportElement.isValid()) {
            throw new InvalidStateException("Invalid PackPlanImportElement: " + packPlanImportElement.toString());
        }
        return packPlanImportElement;
    }

    private List<ItemImportElement> parseForItemImportElements(File file) throws IOException {
        final FileReader fileReader = new FileReader(file);

        final List<ItemImportElement> itemImportElements = CsvParser.skip(1)
                .mapTo(ItemImportElement.class)
                .headers(ItemImportHeader.ITEM_ID.name(),
                        ItemImportHeader.ITEM_LENGTH.name(),
                        ItemImportHeader.PACK_PLAN_ITEM_QUANTITY.name(),
                        ItemImportHeader.ITEM_WEIGHT.name())
                .stream(fileReader)
                .collect(Collectors.toList());


        for (ItemImportElement itemImportElement : itemImportElements) {
            if (!itemImportElement.isValid()) {
                throw new IllegalStateException("Invalid ItemImportElement: " + itemImportElement.toString());
            }
        }

        return itemImportElements;
    }

    private PackPlan mapToPackPlan(final PackPlanImportElement packPlanImportElement) {
        final PackPlan packPlan = new PackPlan();

        packPlan.setMaxPiecesPerPack(packPlanImportElement.getMaxPiecesPerPack());
        packPlan.setMaxWeightPerPack(packPlanImportElement.getMaxWeightPerPack());
        packPlan.setPackSortOrder(packPlanImportElement.getPackSortOrder());

        return packPlan;
    }

    private Pair<PackPlanItem, Item> mapToPackPlanItemAndItem(final PackPlan packPlan, final ItemImportElement itemImportElement) {
        final Item item = new Item();
        item.setId(itemImportElement.getItemId());
        item.setLength(itemImportElement.getItemLength());
        item.setWeight(itemImportElement.getItemWeight());

        final PackPlanItem packPlanItem = new PackPlanItem();
        packPlanItem.setItem(item);
        packPlanItem.setPackPlan(packPlan);
        packPlanItem.setQuantity(itemImportElement.getPackPlanItemQuantity());

        return Pair.of(packPlanItem, item);
    }

    private List<Pair<PackPlanItem, Item>> mapToPackPlanItemAndItems(final PackPlan packPlan, final List<ItemImportElement> itemImportElements) {
        return itemImportElements.stream()
                .map(itemImportElement -> mapToPackPlanItemAndItem(packPlan, itemImportElement))
                .collect(Collectors.toList());
    }

}

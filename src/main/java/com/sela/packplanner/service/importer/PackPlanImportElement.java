package com.sela.packplanner.service.importer;

import com.sela.packplanner.model.packplan.PackSortOrder;

public class PackPlanImportElement {

    private String sortOrder;

    private Long maxPiecesPerPack;

    private Double maxWeightPerPack;

    public PackPlanImportElement() {
    }

    public PackPlanImportElement(String sortOrder, Long maxPiecesPerPack, Double maxWeightPerPack) {
        this.sortOrder = sortOrder;
        this.maxPiecesPerPack = maxPiecesPerPack;
        this.maxWeightPerPack = maxWeightPerPack;
    }
    public PackSortOrder getPackSortOrder() {
        return PackSortOrder.findByName(getSortOrder());
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Long getMaxPiecesPerPack() {
        return maxPiecesPerPack;
    }

    public void setMaxPiecesPerPack(Long maxPiecesPerPack) {
        this.maxPiecesPerPack = maxPiecesPerPack;
    }

    public Double getMaxWeightPerPack() {
        return maxWeightPerPack;
    }

    public void setMaxWeightPerPack(Double maxWeightPerPack) {
        this.maxWeightPerPack = maxWeightPerPack;
    }

    @Override
    public String toString() {
        return "PackPlanImportElement{" +
                "sortOrder='" + sortOrder + '\'' +
                ", maxPiecesPerPack=" + maxPiecesPerPack +
                ", maxWeightPerPack=" + maxWeightPerPack +
                '}';
    }
    public boolean isValid(){
        if(sortOrder == null || maxPiecesPerPack == null || maxWeightPerPack == null){
            return false;
        }

        if(PackSortOrder.findByName(sortOrder) == null){
            return false;
        }
        return true;
    }
}

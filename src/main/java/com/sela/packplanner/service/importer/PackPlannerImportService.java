package com.sela.packplanner.service.importer;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface PackPlannerImportService {

    void importPackPlan(File file) throws IOException;
}

package com.sela.packplanner.service.importer;

public enum ItemImportHeader {
    ITEM_ID,

    ITEM_LENGTH,

    PACK_PLAN_ITEM_QUANTITY,

    ITEM_WEIGHT;
}

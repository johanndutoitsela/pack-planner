package com.sela.packplanner.service.importer;

public enum PackPlanImportHeader {
    SORT_ORDER,
    MAX_PIECES_PER_PACK,
    MAX_WEIGHT_PER_PACK;
}

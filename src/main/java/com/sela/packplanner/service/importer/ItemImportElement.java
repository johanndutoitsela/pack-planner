package com.sela.packplanner.service.importer;

public class ItemImportElement {
    private String itemId;

    private Long itemLength;

    private Long packPlanItemQuantity;

    private Double itemWeight;

    public ItemImportElement() {
    }

    public ItemImportElement(String itemId, Long itemLength, Long packPlanItemQuantity, Double itemWeight) {
        this.itemId = itemId;
        this.itemLength = itemLength;
        this.packPlanItemQuantity = packPlanItemQuantity;
        this.itemWeight = itemWeight;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Long getItemLength() {
        return itemLength;
    }

    public void setItemLength(Long itemLength) {
        this.itemLength = itemLength;
    }

    public Long getPackPlanItemQuantity() {
        return packPlanItemQuantity;
    }

    public void setPackPlanItemQuantity(Long packPlanItemQuantity) {
        this.packPlanItemQuantity = packPlanItemQuantity;
    }

    public Double getItemWeight() {
        return itemWeight;
    }

    public void setItemWeight(Double itemWeight) {
        this.itemWeight = itemWeight;
    }

    @Override
    public String toString() {
        return "ItemImportElement{" +
                "itemId='" + itemId + '\'' +
                ", itemLength=" + itemLength +
                ", packPlanItemQuantity=" + packPlanItemQuantity +
                ", itemWeight=" + itemWeight +
                '}';
    }

    public boolean isValid(){
        if(itemId == null || itemLength == null || packPlanItemQuantity == null || itemWeight == null){
            return false;
        }
        return true;
    }
}

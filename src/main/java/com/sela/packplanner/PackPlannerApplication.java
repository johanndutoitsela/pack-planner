package com.sela.packplanner;

import com.sela.packplanner.service.importer.PackPlannerImportService;
import org.simpleflatmapper.csv.CsvParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.Arrays;

@SpringBootApplication
public class PackPlannerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PackPlannerApplication.class, args);
    }


    @Autowired
    PackPlannerImportService packPlannerImportService;

    @Override
    public void run(String... args) throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("InputElements_10.csv").getFile());

        this.packPlannerImportService.importPackPlan(file);

        CsvParser.forEach(file, row -> System.out.println(Arrays.toString(row)));
    }
}
package com.sela.packplanner.model.item;

import javax.persistence.*;

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long key;

    @Column(unique=true)
    private String id;

    private long length;

    private double weight;

    public Item() {
    }

    public Item(String id, long length, double weight) {
        this.id = id;
        this.length = length;
        this.weight = weight;
    }

    public long getKey() {
        return key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }
}

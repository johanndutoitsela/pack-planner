package com.sela.packplanner.model.packplan;

import javax.persistence.*;

@Entity
public class PackPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long key;

    @Enumerated(EnumType.STRING)
    private PackSortOrder packSortOrder;

    private long maxPiecesPerPack;

    private double maxWeightPerPack;

    public PackPlan() {
    }

    public PackPlan(PackSortOrder packSortOrder, long maxPiecesPerPack, double maxWeightPerPack) {
        this.packSortOrder = packSortOrder;
        this.maxPiecesPerPack = maxPiecesPerPack;
        this.maxWeightPerPack = maxWeightPerPack;
    }

    public long getKey() {
        return key;
    }

    public double getMaxWeightPerPack() {
        return maxWeightPerPack;
    }

    public void setMaxWeightPerPack(double maxWeightPerPack) {
        this.maxWeightPerPack = maxWeightPerPack;
    }

    public long getMaxPiecesPerPack() {
        return maxPiecesPerPack;
    }

    public void setMaxPiecesPerPack(long maxPiecesPerPack) {
        this.maxPiecesPerPack = maxPiecesPerPack;
    }

    public PackSortOrder getPackSortOrder() {
        return packSortOrder;
    }

    public void setPackSortOrder(PackSortOrder packSortOrder) {
        this.packSortOrder = packSortOrder;
    }
}

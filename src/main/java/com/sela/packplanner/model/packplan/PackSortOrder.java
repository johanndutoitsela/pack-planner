package com.sela.packplanner.model.packplan;

import java.util.Arrays;

public enum PackSortOrder {

    NATURAL,

    SHORT_TO_LONG,

    LONG_TO_SHORT;

   public static PackSortOrder findByName(String name){
       return Arrays.stream(values())
               .filter(packSortOrder -> packSortOrder.name().equals(name))
               .findFirst()
               .orElse(null);
   }
}

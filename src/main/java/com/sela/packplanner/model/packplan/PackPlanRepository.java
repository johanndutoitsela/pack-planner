package com.sela.packplanner.model.packplan;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PackPlanRepository extends JpaRepository<PackPlan, Long> {
}

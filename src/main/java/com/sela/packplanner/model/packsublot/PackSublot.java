package com.sela.packplanner.model.packsublot;

import com.sela.packplanner.model.item.Item;
import com.sela.packplanner.model.pack.Pack;

import javax.persistence.*;

@Entity
public class PackSublot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long key;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Pack pack;

    private long packPosition;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Item item;

    private long quantity;

    public PackSublot() {
    }

    public PackSublot(Pack pack, long packPosition, Item item, long quantity) {
        this.pack = pack;
        this.packPosition = packPosition;
        this.item = item;
        this.quantity = quantity;
    }

    public long getKey() {
        return key;
    }

    public Pack getPack() {
        return pack;
    }

    public void setPack(Pack pack) {
        this.pack = pack;
    }

    public long getPackPosition() {
        return packPosition;
    }

    public void setPackPosition(long packPosition) {
        this.packPosition = packPosition;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}

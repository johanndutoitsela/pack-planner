package com.sela.packplanner.model.packsublot;

import com.sela.packplanner.model.pack.Pack;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PackSublotRepository extends JpaRepository<PackSublot, Long> {

    List<PackSublot> findByPack(final Pack pack);
}

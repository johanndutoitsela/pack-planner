package com.sela.packplanner.model.pack;

import com.sela.packplanner.model.packplan.PackPlan;
import com.sela.packplanner.model.packsublot.PackSublot;

import javax.persistence.*;
import java.util.List;

@Entity
public class Pack {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long key;

    private long number;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private PackPlan packPlan;

    @OneToMany(mappedBy = "pack", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PackSublot> packSublots;

    public Pack() {
    }

    public Pack(long number, PackPlan packPlan) {
        this.number = number;
        this.packPlan = packPlan;
    }

    public long getKey() {
        return key;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public PackPlan getPackPlan() {
        return packPlan;
    }

    public void setPackPlan(PackPlan packPlan) {
        this.packPlan = packPlan;
    }
}

package com.sela.packplanner.model.pack;

import com.sela.packplanner.model.packplan.PackPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PackRepository extends JpaRepository<Pack, Long> {

    List<Pack> findByPackPlan(final PackPlan packPlan);
}

package com.sela.packplanner.model.packplanitem;

import com.sela.packplanner.model.packplan.PackPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PackPlanItemRepository extends JpaRepository<PackPlanItem, Long> {

    List<PackPlanItem> findByPackPlan(final PackPlan packPlan);
}

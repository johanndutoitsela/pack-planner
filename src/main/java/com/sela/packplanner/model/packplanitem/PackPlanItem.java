package com.sela.packplanner.model.packplanitem;

import com.sela.packplanner.model.item.Item;
import com.sela.packplanner.model.packplan.PackPlan;

import javax.persistence.*;

@Entity
public class PackPlanItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long key;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private PackPlan packPlan;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Item item;

    private long quantity;

    public PackPlanItem() {
    }

    public PackPlanItem(PackPlan packPlan, Item item, long quantity) {
        this.packPlan = packPlan;
        this.item = item;
        this.quantity = quantity;
    }

    public long getKey() {
        return key;
    }

    public PackPlan getPackPlan() {
        return packPlan;
    }

    public void setPackPlan(PackPlan packPlan) {
        this.packPlan = packPlan;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
